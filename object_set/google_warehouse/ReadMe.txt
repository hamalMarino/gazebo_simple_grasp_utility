This is a porting of objects from the Google warehouse set, cleaned for the PaCMan project: the .zip file is downloadable from
https://github.com/pacman-project/Vision/blob/master/data/Warehouse_Clean_Vladislav_full.zip

To port the objects in Gazebo-ready format, .obj files have to be converted (in .stl format), the frame origin has to be changed to the lowest object point, and faces have to be reoriented in some cases to always have outward normals.
Moreover, the scale needs to be adjusted, from millimeters to meters (I actually applied a 0.0012 scale transform to all meshes, which is not exactly from mm to m: this came from the 300mm_frying_pan object, which was otherwise 250mm, and was used as a measure).

I needed to apply an additional scale of 0.75 to carafe.stl to make it more realistically scaled.
As grasping missingCoffeeMug1 was not working, I tried adding a small_carafe.stl, scaled with a factor of 0.75 from carafe.stl.
