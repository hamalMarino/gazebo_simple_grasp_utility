/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#ifndef GAZEBO_SIMPLE_GRASP
#define GAZEBO_SIMPLE_GRASP

#include <iostream>
#include <string>
#include <vector>
#include <kdl/frames.hpp>
// #include <thread>
#include <mutex>

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/WorldState.h>
#include <rosbag/bag.h>
// gazebo SH control
#include "gazebo_simple_grasp_utility/gazebo_simple_hand_control.h"

class GazeboSimpleGrasp
{
public:
    /**
     * @brief Constructor
     * 
     * @param freq_multiplier multiplier to scale the timestep of the simulation (w.r.t. default 1ms); e.g., freq_multiplier=10 means that the timestep will be 1ms/10=0.1ms
     */
    GazeboSimpleGrasp(double freq_multiplier);
    ~GazeboSimpleGrasp();
    
    /**
     * @brief Specify topics and other variables needed to control the hand in Gazebo
     * 
     * @param pose_topic topic to use in order to update the hand pose
     * @param joint_topic topic to use in order to update the joint value(s)
     * @param joint_names vector of joint names to be used when controlling the hand
     * @param ts value for the sample time to use throughout
     */
    void setHandParameters(const std::string& pose_topic, const std::string& joint_topic, const std::vector<std::string>& joint_names, double ts);
    
    /**
     * @brief Do a full grasp run, setting waypoints for the wrist and for the all the joints, giving the timing of these waypoints as well
     * 
     * @param bag_name name of the bag to be used for recordin purposes
     * @param vec_frames waypoints (poses) for the hand
     * @param vec_joints waypoints (angles) for the hand commanded joint(s)
     * @param dts delta timestamp, time difference between successive waypoints
     * 
     * @return true if the TRIAL was performed and recorded successfully; false otherwise. Note, it does not consider whether the GRASP was successful.
     */
    bool doSingleGraspRun(const std::string& bag_name, const std::vector< KDL::Frame >& vec_frames, const std::vector< std::vector< double > >& vec_joints, const std::vector< double >& dts);
    
    /**
     * @brief Specify an object to be used in simulation via its name
     * Specify an object to be used in simulation via its name; the object will be spawned in Gazebo, assuming a parameter exist on the server which is ${obj_name}_description
     * 
     * @param obj_name reference name for the object model
     * 
     * @return whether the object was successfully spawned or not
     */
    bool setObjectByName(std::string obj_name, const KDL::Frame& object_init_pose = KDL::Frame::Identity(), bool add_back_gravity = true );
    
private:
    ros::NodeHandle node_;
    ros::Subscriber link_sub,joint_sub;
    ros::Publisher my_world_pub;
    ros::Publisher screenshot_start_pub_,screenshot_stop_pub_;
    ros::ServiceClient screenshot_delete_client_;
    rosbag::Bag bag;
    int count_l=0,count_j=0,count_wj=0;
    std::mutex bag_mutex;
    double ts_,freq_multiplier_;
    KDL::Frame object_init_pose_;
    
    gazebo_msgs::WorldState ws_buff;
    sensor_msgs::JointState js_buff;
    
    boost::shared_ptr<GazeboSimpleHandControl> GSHC;
    
    void init();
    bool recordBag(const gazebo_msgs::WorldState& ws, const sensor_msgs::JointState& js);
    void callback_link(const gazebo_msgs::LinkStatesConstPtr& links);
    void callback_joints(const sensor_msgs::JointStateConstPtr& joints);
    void changePhysicsProperties(double freq_multiplier);
    void spawnObject(const std::string& object_xml, const KDL::Frame& init_pose = KDL::Frame::Identity(), bool add_back_gravity = true);
    void deleteObject();
};

#endif // GAZEBO_SIMPLE_GRASP