import rosbag
import sys
import rospy
from optparse import OptionParser

# Option parsing
parser = OptionParser()
parser.add_option("-i", "--input_file", dest="input_file",
                  help="input bag FILE",
                  metavar="FILE", action="store")
parser.add_option("-o", "--output_file", dest="output_file",
                  help="output bag FILE", metavar="FILE")
parser.add_option("-d", "--decim_time", dest="decim_time",
                  help="decimation time (resulting bag will have one message every decim_time seconds in the original file)", type="float", metavar="INT")

(options, _) = parser.parse_args()

# Input validation
if options.input_file is None:
    rospy.logerr("The input bag has to be specified")
    sys.exit()
if options.output_file is None:
    rospy.logerr("The output file has to be specified")
    sys.exit()

time_map = {}
d = rospy.Duration.from_sec(options.decim_time)

with rosbag.Bag(options.output_file, 'w') as outbag:
    for topic, msg, t in rosbag.Bag(options.input_file).read_messages():
    	if not topic in time_map:
    		time_map[topic] = t - d - d
        if t < time_map[topic] + d:
            continue
        time_map[topic] = t
        outbag.write(topic, msg, t)