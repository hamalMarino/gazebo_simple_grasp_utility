/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#ifndef GAZEBO_SIMPLE_HAND_CONTROL
#define GAZEBO_SIMPLE_HAND_CONTROL

#include <iostream>
#include <string>
#include <vector>
#include <kdl/frames.hpp>
#include <thread>
#include <mutex>

#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <gazebo_msgs/ModelState.h>

class GazeboSimpleHandControl
{
public:
    /**
     * @brief Constructor
     * 
     * @param pose_topic topic to use in order to update the hand pose
     * @param joint_topic topic to use in order to update the joint value(s)
     * @param ts value for the sample time to use throughout
     */
    GazeboSimpleHandControl(const std::string& pose_topic, const std::string& joint_topic, const std::vector<std::string>& joint_names, double ts);
    ~GazeboSimpleHandControl();
    
    /**
     * @brief Specify a pose and joint values for the hand
     * Specify a pose and joint values for the hand; they will be directly applied
     * 
     * @param ms model state containing the desired pose of the hand (other information will be stored)
     * @param j complete joint vector to be specified for the hand
     */
    void setHandState(const gazebo_msgs::ModelState& ms, const std::vector<double>& j);
    
    /**
     * @brief Specify a pose for a model and starts publishing it
     * Specify a pose for a model; it will be directly applied
     * 
     * @param name reference name for the model (to later overwrite or clear its reference)
     * @param ms model state containing the desired pose of a gazebo model
     */
    void setModelState(const std::string& name, const gazebo_msgs::ModelState& ms);
    
    /**
     * @brief Stop enforcing all model poses (apart from the hand)
     * 
     * @param name reference name for the model whose pose has to be cleared
     */
    void clearModelState(const std::string& name);
    
    /**
     * @brief Stop enforcing all model poses (apart from the hand)
     */
    void clearModelStates();
    
    /**
     * @brief Function to set a sequence of waypoints (pairs of poses @param ps and joint vectors @param js), which will be interpolated linearly, using durations specified by @param dts
     * 
     * @param ps sequence of N waypoints to be traversed
     * @param js sequence of N joint vectors for the hand
     * @param dts (N-1) durations specifying how long each waypoint should last (this will be interpolated using the timestep given in the constructor)
     */
    void setLinearWaypoints(const std::vector<KDL::Frame>& ps, const std::vector<std::vector<double>>& js, const std::vector<double>& dts);
    
    /**
     * @brief Stop publishing the hand state
     * Stop the publisher and the linear interpolator.
     * IMPORTANT: after calling this function and before setting a new hand state, make sure to sleep for a small amount in order for the threads to exit properly (e.g. ros::Duration(0.1).sleep())
     */
    void stop();
    
private:
    ros::Publisher pose_pub_, joint_pub_;
    double ts_;
    trajectory_msgs::JointTrajectory j_traj_;
    gazebo_msgs::ModelState ms_;
    std::map<std::string,gazebo_msgs::ModelState> model_ms_map_;
    std::thread *pub_thread_=NULL, *lin_wp_thread_=NULL;
    std::mutex pub_mutex_;
    bool lin_wp_stopping_,pub_stopping_;
    
    void setLinearWaypoints_impl(const std::vector<KDL::Frame>& ps, const std::vector<std::vector<double>>& js, const std::vector<double>& dts);
    void publish_messages();
    void stopLinWP();
    void stopPub();
};

#endif // GAZEBO_SIMPLE_HAND_CONTROL