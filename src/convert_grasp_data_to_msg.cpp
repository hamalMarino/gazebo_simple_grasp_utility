/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include "dual_manipulation_shared/grasp_trajectory.h"
#include "dual_manipulation_shared/serialization_utils.h"
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>
#include <iomanip>
#include <vector>

void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts);
void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::string& obj_name, int& obj_id, int& grasp_id, bool&, KDL::Frame&);
void parseParameters(XmlRpc::XmlRpcValue& params, std::vector<std::string>& grasp_names, std::vector<std::string>& joint_names, std::string&, double&, bool&);

bool convertGraspDataToMsg(int obj_id, const std::vector<KDL::Frame>& vec_frames, const std::vector<std::string>& joint_names, const std::vector<std::vector<double>>& vec_joints, const std::vector<double>& dts, dual_manipulation_shared::grasp_trajectory& grasp_msg);
bool convertGraspMsgToData(int obj_id, int grasp_id, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts);

int main(int argc, char** argv)
{
    ros::init(argc,argv,"gazebo_simple_grasp_converter");
    
    ros::NodeHandle node_;
    
    char mode;
    
    while(mode != '1' && mode != '2')
    {
        std::cout << "Do you want to convert data>msg [1] or msg>data [2]?" << std::endl;
        std::cin >> mode;
        if(mode != '1' && mode != '2')
            std::cout << "Invalid choice \'" << mode << "\', try again..." << std::endl;
    }
    
    // parse parameters from server
    std::vector<std::string> grasp_names;
    std::vector<std::string> joint_names;
    
    XmlRpc::XmlRpcValue params;
    if (node_.getParam("gazebo_simple_grasp_utility", params))
    {
        std::string s_tmp;
        double d_tmp;
        bool b_tmp;
        parseParameters(params,grasp_names,joint_names,s_tmp,d_tmp,b_tmp);
    }
    else
    {
        ROS_ERROR_STREAM("Could not read parameters from server: returning!");
        return -1;
    }
    
    for(auto grasp_name:grasp_names)
    {
        // parse parameters from server
        std::string obj_name;
        std::vector<KDL::Frame> vec_frames;
        std::vector<std::vector<double>> vec_joints;
        std::vector<double> dts;
        int obj_id,grasp_id;
        bool tmp;
        KDL::Frame f_tmp;
        parseParameters(params,grasp_name,obj_name,obj_id,grasp_id,tmp,f_tmp);
        parseParameters(params,grasp_name,vec_frames,vec_joints,dts);
        
        dual_manipulation_shared::grasp_trajectory grasp_msg;
        std::string file_name = "object" + std::to_string( obj_id ) + "/grasp" + std::to_string( grasp_id );
        
        if (mode=='1')
        {
            convertGraspDataToMsg(obj_id,vec_frames,joint_names,vec_joints,dts,grasp_msg);
            
            if(!serialize_ik(grasp_msg,file_name))
            {
                ROS_ERROR_STREAM("Unable to serialize \'" << file_name << "\' - returning.");
                return -2;
            }
            else
                ROS_INFO_STREAM("Successfully serialized \'" << file_name << "\'!");
        }
        else if (mode == '2')
        {
            if(!convertGraspMsgToData(obj_id, grasp_id, vec_frames, vec_joints, dts))
            {
                return -2;
            }
            
            // // output values in this format (4 and 6 spaces to indent)
            //     scaling_pose: [0.0,0.0,0.0, 0.0,0.0,0.0]
            //     wp0:
            //       pose: [+0.0,-0.283,+0.288, +0.0,+1.57079,+1.57079]
            //       joints: [0.0]
            //       time_from_previous: 0.0
            
            // the scaling pose gets always reset by this procedure (embedded into the other frames)
            std::cout << "    scaling_pose: [0.0,0.0,0.0, 0.0,0.0,0.0]" << std::endl;
            
            // print waypoints
            for(int i=0; i<vec_frames.size(); i++)
            {
                std::cout << "    wp" << i << ":" << std::endl;
                KDL::Frame& f(vec_frames.at(i));
                std::cout << "      pose: [" << f.p[0] << "," << f.p[1] << "," << f.p[2] << ", ";
                double r,p,y;
                f.M.GetRPY(r,p,y);
                std::cout << r << "," << p << "," << y << "]" << std::endl;
                
                std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(2);

                std::cout << "      joints: [";
                for(auto j:vec_joints.at(i))
                    std::cout << j << " ";
                std::cout << "]" << std::endl;
                if(i==0)
                    std::cout << "      time_from_previous: 0.0" << std::endl;
                else
                    std::cout << "      time_from_previous: " << dts.at(i-1) << std::endl;
                
                std::cout << std::resetiosflags(std::ios::fixed) << std::setprecision(6);
            }
        }
    
    }
    
    return 0;
}
