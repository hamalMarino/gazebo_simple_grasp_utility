/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include "dual_manipulation_shared/parsing_utils.h"
#include <XmlRpc.h>
#include <kdl/frames.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <complex>
#include <kdl/frames_io.hpp>
#include <XmlRpcValue.h>

#define DEBUG 0

void parsePose(XmlRpc::XmlRpcValue& params, const std::string& pose_name, KDL::Frame& pose)
{
    std::vector<double> sp;
    parseSingleParameter(params,sp,pose_name,6);
    
    if(sp.size() != 0 && sp.size() != 6 && sp.size() != 7)
    {
        std::cout << "[FATAL] The size of the pose should either be 6 (Position + RPY) or 7 (Position + Quaternion [x,y,z,w]), while it is " << sp.size() << " - returning!" << std::endl;
        exit(-1);
    }
    
    if(sp.size() == 0)
        pose = KDL::Frame::Identity();
    else if(sp.size() == 6)
        pose = KDL::Frame(KDL::Rotation::RPY(sp.at(3),sp.at(4),sp.at(5)),KDL::Vector(sp.at(0),sp.at(1),sp.at(2)));
    else
    {
        // normalize quaternion
        double norm = std::sqrt(sp.at(3)*sp.at(3)+sp.at(4)*sp.at(4)+sp.at(5)*sp.at(5)+sp.at(6)*sp.at(6));
        pose = KDL::Frame(KDL::Rotation::Quaternion(sp.at(3)/norm,sp.at(4)/norm,sp.at(5)/norm,sp.at(6)/norm),KDL::Vector(sp.at(0),sp.at(1),sp.at(2)));
    }
    
}

void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts)
{
    assert(params.getType() == XmlRpc::XmlRpcValue::TypeStruct);
    
    // clear stuff
    vec_frames.clear();
    vec_joints.clear();
    dts.clear();
    
    KDL::Frame scale_f;
    parsePose(params[grasp_name],"scaling_pose",scale_f);
    
#if DEBUG
    std::cout << "scaling pose:" << scale_f << std::endl;
#endif
    
    int num_wp = 0;
    
    while(params[grasp_name].hasMember("wp" + std::to_string(num_wp)))
    {
//         std::vector<double> pose; // either 6 or 7 numbers, specifying a 3D point and RPY or Quaternion rotation
        std::vector<double> joints; // joints vector (any number)
        double ts;
        
        KDL::Frame tmp_frame;
        parsePose(params[grasp_name]["wp" + std::to_string(num_wp)],"pose",tmp_frame);
        parseSingleParameter(params[grasp_name]["wp" + std::to_string(num_wp)],joints,"joints");
        parseSingleParameter(params[grasp_name]["wp" + std::to_string(num_wp)],ts,"time_from_previous");
        
        vec_frames.push_back(scale_f*tmp_frame);
        vec_joints.emplace_back(joints);
        dts.push_back(ts);
        
#if DEBUG
        std::cout << "wp #" << num_wp << ": ";
        std::cout << tmp_frame << std::endl;
        for(auto w:joints)
            std::cout << w << " | ";
        std::cout << std::endl;
        std::cout << ts << std::endl << std::endl;
#endif
        
        num_wp++;
    }
    
    if(!dts.empty())
        dts.erase(dts.begin());
    
#if DEBUG
    std::cout << "Read #" << num_wp << " waypoints!" << std::endl;
    
    std::cout << "dts: | ";
    for(auto dt:dts)
        std::cout << dt << " | ";
    std::cout << std::endl;
#endif
}

void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::string& obj_name, int& obj_id, int& grasp_id, bool& use_db, KDL::Frame& scaling_pose)
{
    parseSingleParameter(params[grasp_name],obj_name,"obj_name");
    parseSingleParameter(params[grasp_name],obj_id,"obj_id");
    parseSingleParameter(params[grasp_name],grasp_id,"grasp_id");
    parseSingleParameter(params[grasp_name],use_db,"use_db");
    parsePose(params[grasp_name],"scaling_pose",scaling_pose);
}

void parseParameters(XmlRpc::XmlRpcValue& params, std::vector<std::string>& grasp_names, std::vector<std::string>& joint_names, std::string& hand_joint_topic, double& multiplier, bool& add_back_gravity)
{
    parseSingleParameter(params,grasp_names,"grasp_names");
    parseSingleParameter(params,joint_names,"hand_actuated_joints");
    parseSingleParameter(params,hand_joint_topic,"hand_joint_topic");
    parseSingleParameter(params,multiplier,"frequency_multiplier");
    parseSingleParameter(params,add_back_gravity,"add_back_gravity");
}