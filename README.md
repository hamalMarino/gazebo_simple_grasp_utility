# gazebo_simple_grasp_utility

This repository helps in generating grasps within Gazebo.

## Dependencies

You will need the repositories for the hand you want to use: this code has initially been developed for use with the [`pisa-iit-soft-hand`](https://github.com/CentroEPiaggio/pisa-iit-soft-hand).
Moreover, you will also need [`dual_manipulation_shared`](https://bitbucket.org/dualmanipulation/shared) for various definitions and the `grasp_trajectory` therein, and an object database such as the [`pacman-object-database`](https://github.com/pacman-project/pacman-object-database).
Also [`dual_manipulation_grasp_db`](https://bitbucket.org/dualmanipulation/grasp_db) is suggested in order to use this in synergy with GMU.

## Useful commands

To use this utility, you will need two shells, one to start Gazebo (in paused mode), the other to then launch the utility. The simulation will start automatically.
The bag file will be recorded in `~/.ros/` folder. Utilities are provided to bring all the data together, also converting from .bag to .hdf5 files. Another conversion to dumped txt files only works if using the `pisa-iit-soft-hand`.

```
shell_1$ roslaunch gazebo_simple_grasp_utility loadScene.launch
shell_2$ roslaunch gazebo_simple_grasp_utility gazebo_simple_grasp_utility.launch
```


One more thing you can do is to convert between the simple data format used in here to (and then from) a `grasp_msg` format, which could then be adapted using for example the `Grasp Modification Utility` in `dual_manipulation_grasp_db`.

```
roslaunch gazebo_simple_grasp_utility convert_grasp_data_to_msg.launch
```


If you want to change the hand used in the simulation, you will need to change accordingly the following files:
- config/controllers.yaml
- config/joint_names.yaml
- config/simple_grasp_parameters.yaml >> the hand parameters such as actuated joints and topics
- urdf/hand.urdf.xacro >> the hand declaration part

To add new object, modify the database package://dual_manipulation_grasp_db/objects_only_simple_grasp.db to contain needed data for the new object.
