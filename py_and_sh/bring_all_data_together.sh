# bring_all_data_together.sh serves as a simple mean to move stuff from various places where they are generated using the utility, into a single folder of your choice
# 
# You need to adjust the variable DESTINATION if you want to use different paths for storing data

# where to put everything once it's together
DESTINATION=~/Desktop/SimpleGrasp/Data_various

# select which one is the last log folder (could be probably done better)
for i in ~/.gazebo/log/*; do
	LATEST_GAZEBO_LOG_FOLDER=$i
done

# move all bag files to the log folder
for i in ~/.ros/*.bag; do
	mv $i $LATEST_GAZEBO_LOG_FOLDER
done

# move screenshots to the log folder
mv ~/Pictures/gazebo_screenshots $LATEST_GAZEBO_LOG_FOLDER

# convert bag files to hdf5 and dump them
prefix=""; suffix=".bag";
for i in $LATEST_GAZEBO_LOG_FOLDER/*.bag; do
	F_NAME=$i; F_NAME=${F_NAME#$prefix}; F_NAME=${F_NAME%$suffix};
	python ./gazebosim2HDF5.py -i $F_NAME".bag" -o $F_NAME".hdf5"; sh hdf5dumpall.sh $F_NAME;
done

# move the log folder to the desired destination
mv $LATEST_GAZEBO_LOG_FOLDER $DESTINATION
