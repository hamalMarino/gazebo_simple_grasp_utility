# shell 1 - launch gazebo with the hand spawned
roslaunch gazebo_simple_grasp_utility loadScene.launch

# shell 2 - after choosing the grasp to perform in simple_grasp_parameters.yaml, launch the simulation(s)
roslaunch gazebo_simple_grasp_utility gazebo_simple_grasp_utility.launch

# shell 3 - go in the appropriate place, and collect all the data in the same folder (default: ~/Desktop/SimpleGrasp/Data_various)
# Note that this also converts bag files into hdf5, and dumps them
roscd gazebo_simple_grasp_utility/py_and_sh/
sh ./bring_all_data_together.sh

### Other commands

# conversion of the bag files into hdf5, then dump
# NOTE: this syntax doesn't support spaces in file names!
cd $WHERE_SCRIPTS_ARE
here=$(pwd); cd ~/.ros/; F_NAMES=$(ls *.bag); cd $here; prefix=""; suffix=".bag";
for F_NAME in $F_NAMES; do F_NAME=${F_NAME#$prefix}; F_NAME=${F_NAME%$suffix}; mv ~/.ros/$F_NAME.bag ./; python ./gazebosim2HDF5.py -i $F_NAME".bag" -o $F_NAME".hdf5"; sh hdf5dumpall.sh $F_NAME; done

# conversion of the bag files into hdf5, then dump (single file)
cd /home/viki/Code/bin/data
F_NAME="fixed_obj_rim_grasp"; mv ~/.ros/$F_NAME.bag ./; python ./gazebosim2HDF5.py -i $F_NAME".bag" -o $F_NAME".hdf5"; sh hdf5dumpall.sh $F_NAME

# play the data using GraspPlayer
cd /home/viki/Code/bin
./GraspPlayer ./GraspPlayer_RobotPISASoftHand.xml

## to remove the plane in gazebo (check whether the grasp was successfull)
# rosservice call /gazebo/delete_model '{model_name: ground_plane}'