/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include <iostream>
#include <stdio.h>
#include "ros/ros.h"
#include <string>
#include <XmlRpc.h>
#include <std_srvs/Empty.h>
#include <kdl/frames.hpp>

#include "gazebo_simple_grasp_utility/gazebo_simple_grasp.h"
#include "dual_manipulation_shared/serialization_utils.h"

#define Ts 0.01
#define DEBUG 0
#define TEST_FIXED_VARIABILITY 0

// TODO: look into this for buffer overrun related problems...
// #include <rosbag/recorder.h>
//     rosbag::RecorderOptions opt;
//     opt.compression = rosbag::CompressionType::BZ2;
//     opt.buffer_size = 0; // 0 should stand for infinite
//     opt.name = bag_name;
//     // opt.node;
//     // //
//     rosbag::Recorder rec(opt);
//     rec.Bag::write<>();

// NOTE: synchronization stuff, not actually used in the end
// #include <message_filters/subscriber.h>
// #include <message_filters/time_synchronizer.h>
//     // synchronize stuff
//     message_filters::Subscriber<gazebo_msgs::WorldState> world_sync;
//     message_filters::Subscriber<sensor_msgs::JointState> joint_sync;
//     world_sync.subscribe(node_,"/my_gazebo/world_state",10);
//     joint_sync.subscribe(node_,"/soft_hand/joint_states",10);
//     boost::shared_ptr<message_filters::TimeSynchronizer<gazebo_msgs::WorldState, sensor_msgs::JointState>> sync;
// 
//     sync = boost::shared_ptr<message_filters::TimeSynchronizer<gazebo_msgs::WorldState, sensor_msgs::JointState>>(new message_filters::TimeSynchronizer<gazebo_msgs::WorldState, sensor_msgs::JointState>(world_sync, joint_sync, 10));
//     sync->registerCallback(boost::bind(&recordBag, _1, _2));

void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts);
void parseParameters(XmlRpc::XmlRpcValue& params, const std::string& grasp_name, std::string& obj_name, int& obj_id, int& grasp_id, bool& use_db, KDL::Frame& scaling_pose);
void parseParameters(XmlRpc::XmlRpcValue& params, std::vector<std::string>& grasp_names, std::vector<std::string>& joint_names, std::string& hand_joint_topic, double& multiplier, bool& add_back_gravity);

bool convertGraspMsgToData(int obj_id, int grasp_id, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts);

int main(int argc, char** argv)
{
    ros::init(argc,argv,"gazebo_simple_grasp_utility");
    
    ros::NodeHandle node_;
    
    // parse parameters from server
    std::vector<std::string> grasp_names;
    std::vector<std::string> joint_names;
    std::string hand_joint_topic;
    double freq_multiplier(1.0);
    bool add_back_gravity;
    
    XmlRpc::XmlRpcValue params;
    if (node_.getParam("gazebo_simple_grasp_utility", params))
    {
        parseParameters(params,grasp_names,joint_names,hand_joint_topic,freq_multiplier,add_back_gravity);
    }
    else
    {
        ROS_ERROR_STREAM("Could not read parameters from server: returning!");
        return -1;
    }
    
    ros::AsyncSpinner spin(1);
    spin.start();
    
    GazeboSimpleGrasp GSG(freq_multiplier);
    GSG.setHandParameters("gazebo/set_model_state",hand_joint_topic,joint_names,Ts);

    for(auto grasp_name:grasp_names)
    {
        // parse parameters from server
        std::string obj_name("containerB");
        std::vector<KDL::Frame> vec_frames;
        std::vector<std::vector<double>> vec_joints;
        std::vector<double> dts;
        bool use_db = false;
        int obj_id = 0;
        int grasp_id = 0;
        KDL::Frame scaling_pose(KDL::Frame::Identity());
        
        parseParameters(params,grasp_name,obj_name,obj_id,grasp_id,use_db,scaling_pose);
        if(!use_db)
            parseParameters(params,grasp_name,vec_frames,vec_joints,dts);
        else
        {
            // read grasp database and put the stuff in the appropriate place, considering also the scaling pose
            convertGraspMsgToData(obj_id,grasp_id,vec_frames,vec_joints,dts);
            for(auto& f:vec_frames)
                f = scaling_pose*f;
        }

        GSG.setObjectByName(obj_name,KDL::Frame::Identity(),add_back_gravity);
        
        // simple way to have some variability in a given grasp: use same waypoints, but include a pre-multiplication of frames in order to obtain different results
        std::vector<KDL::Frame> variability_frames;
        std::vector<std::string> postfixes;
        variability_frames.push_back(KDL::Frame::Identity());
#if TEST_FIXED_VARIABILITY
        variability_frames.push_back(KDL::Frame(KDL::Vector(0,0,0.01)));
        variability_frames.push_back(KDL::Frame(KDL::Vector(0,0,-0.01)));
        variability_frames.push_back(KDL::Frame(KDL::Vector(0,0.01,0)));
        variability_frames.push_back(KDL::Frame(KDL::Vector(0,-0.01,0)));
#endif
        for(int i=0; i < variability_frames.size(); i++)
            postfixes.push_back("_take" + std::to_string(i));
        
        std::cout << variability_frames.size() << " frames, with postfixes:\n";
        for(auto p:postfixes)
            std::cout << p << '\n';
        std::cout << std::endl;
        
#if DEBUG
        std::cout << "\nPress any key to continue..." << std::endl;
        
        char y;
        std::cin >> y;
#endif
        
        // NOTE: this is done in order to have a working reset of the hand configuration at the beginning
        std_srvs::Empty empty;
        ros::service::call<std_srvs::Empty>("gazebo/unpause_physics",empty);
        ros::Duration(0.1).sleep();
        ros::service::call<std_srvs::Empty>("gazebo/pause_physics",empty);
        
        uint max_attempts = 10;
        uint counter = 0;
        for(int i=0; i < variability_frames.size(); i++)
        {
            std::cout << "\nNow going to perform " << postfixes.at(i) << " trial..." << std::endl;
            
            std::vector<KDL::Frame> local_vec_frames(vec_frames);
            for(auto& f:local_vec_frames)
                f = variability_frames.at(i)*f;
            bool ok = GSG.doSingleGraspRun(grasp_name + postfixes.at(i),local_vec_frames,vec_joints,dts);
            counter++;
            if(!ok && counter < max_attempts)
                i--;
            else if(!ok && counter >= max_attempts)
            {
                // in case everything went wrong, rename file and continue
                std::string old_name(grasp_name + postfixes.at(i) + ".bag");
                std::string new_name(grasp_name + postfixes.at(i) + "_not_working.bag");
                int result;
                result = rename(old_name.c_str(), new_name.c_str());
                if ( result == 0 )
                    ROS_DEBUG_STREAM("File successfully renamed \"" << old_name << "\" > \"" << new_name << "\"");
                else
                    ROS_ERROR_STREAM("Error renaming file \"" << old_name << "\"! Make sure to do that manually!");
                counter = 0;
            }
            else // if (ok)
                counter = 0;
        }
    }
    return 0;
}

// TODO: IF the object is not fixed, remove ground_plane, wait some time, then check whether the object is moving (and rename the bag accordingly with "_ok" or "_fail")

