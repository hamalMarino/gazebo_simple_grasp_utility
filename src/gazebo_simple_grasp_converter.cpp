/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include "dual_manipulation_shared/grasp_trajectory.h"
#include "dual_manipulation_shared/serialization_utils.h"
#include <kdl/frames.hpp>
#include <kdl_conversions/kdl_msg.h>
#include <vector>

KDL::Frame kukaCoupler_palm(KDL::Vector(0.002,0.00695,0.0905));

bool convertGraspDataToMsg(int obj_id, const std::vector<KDL::Frame>& vec_frames, const std::vector<std::string>& joint_names, const std::vector<std::vector<double>>& vec_joints, const std::vector<double>& dts, dual_manipulation_shared::grasp_trajectory& grasp_msg)
{
    grasp_msg.object_db_id = obj_id;
    grasp_msg.attObject.object.mesh_poses.resize(1);
    grasp_msg.attObject.object.id = "object";
    tf::poseKDLToMsg(vec_frames.back().Inverse(),grasp_msg.attObject.object.mesh_poses.at(0));
    
    for(auto& f:vec_frames)
    {
        // f: object_kukaCoupler
        // I want p to be object_palm
        geometry_msgs::Pose p;
        tf::poseKDLToMsg(f*kukaCoupler_palm,p);
        grasp_msg.ee_pose.push_back(p);
    }
    
    grasp_msg.grasp_trajectory.joint_names = joint_names;
    
    ros::Duration cumulative_time(0.0);
    for(int i=0; i<vec_joints.size(); i++)
    {
        trajectory_msgs::JointTrajectoryPoint pt;
        if(i>0)
            cumulative_time += ros::Duration(dts.at(i-1));
        pt.time_from_start = cumulative_time;
        pt.positions = vec_joints.at(i);
        grasp_msg.grasp_trajectory.points.push_back(pt);
    }
    
    return true;
}

bool convertGraspMsgToData(int obj_id, int grasp_id, std::vector<KDL::Frame>& vec_frames, std::vector<std::vector<double>>& vec_joints, std::vector<double>& dts)
{
    dual_manipulation_shared::grasp_trajectory grasp_msg;
    std::string file_name = "object" + std::to_string( obj_id ) + "/grasp" + std::to_string( grasp_id );
    
    if(!deserialize_ik(grasp_msg,file_name))
    {
        ROS_ERROR_STREAM("Unable to deserialize \'" << file_name << "\' - returning.");
        return false;
    }
    
    // if this is not true, there is no way to determine what will happen before, so return false
    bool right_size = (grasp_msg.ee_pose.size() == grasp_msg.grasp_trajectory.points.size());
    if(!right_size)
    {
        ROS_ERROR_STREAM("grasp_msg.ee_pose.size() is " << grasp_msg.ee_pose.size() << " while grasp_msg.grasp_trajectory.points.size() is " << grasp_msg.grasp_trajectory.points.size() << ", but they should be the same - returning.");
        return false;
    }
    
    vec_frames.clear();
    vec_joints.clear();
    dts.clear();
    // store waypoints
    for(int i=0; i<grasp_msg.ee_pose.size(); i++)
    {
        auto pose=grasp_msg.ee_pose.at(i);
        KDL::Frame f;
        tf::poseMsgToKDL(pose,f);
        // p is object_palm
        // I want f to be object_kukaCoupler
        f = f*kukaCoupler_palm.Inverse();
        vec_frames.push_back(f);
        vec_joints.push_back(grasp_msg.grasp_trajectory.points.at(i).positions);
        
        auto& pts = grasp_msg.grasp_trajectory.points;
        // the time is always w.r.t. the first frame, but I want the duration: take the diff
        if(i>0)
            dts.push_back(pts.at(i).time_from_start.toSec() - pts.at(i-1).time_from_start.toSec());
    }
    
    return true;
}
