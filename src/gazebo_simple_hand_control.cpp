/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include "gazebo_simple_grasp_utility/gazebo_simple_hand_control.h"

#include <kdl_conversions/kdl_msg.h>
#include <kdl/frames_io.hpp>
#include <tf/transform_broadcaster.h>

#define CLASS_NAMESPACE "GazeboSimpleHandControl::"
#define DEBUG 0

GazeboSimpleHandControl::GazeboSimpleHandControl(const std::string& pose_topic, const std::string& joint_topic, const std::vector< std::string >& joint_names, double ts)
{
    ros::NodeHandle n;
    
    pose_pub_ = n.advertise<gazebo_msgs::ModelState>(pose_topic,0);
    joint_pub_ = n.advertise<trajectory_msgs::JointTrajectory>(joint_topic,0);
    ts_ = ts;
    j_traj_.joint_names = joint_names;
}

GazeboSimpleHandControl::~GazeboSimpleHandControl()
{
    this->stop();
    pose_pub_.shutdown();
    joint_pub_.shutdown();
}

void GazeboSimpleHandControl::setHandState(const gazebo_msgs::ModelState& ms, const std::vector< double >& j)
{
    std::unique_lock<std::mutex>(pub_mutex_);
    
    // set hand pose, having at first also other useful information
    ms_ = ms;
    
    // set hand joint trajectory
    j_traj_.points.clear();
    
    // ask to place the hand in the desired configuration asap
    trajectory_msgs::JointTrajectoryPoint pt;
    pt.positions = j;
    pt.time_from_start = ros::Duration(ts_);
    j_traj_.points.push_back(pt);
    
    stopPub();
    pub_thread_ = new std::thread(&GazeboSimpleHandControl::publish_messages, this);
}

void GazeboSimpleHandControl::setModelState(const std::string& name, const gazebo_msgs::ModelState& ms)
{
    std::unique_lock<std::mutex>(pub_mutex_);
    
    model_ms_map_[name] = ms;
}

void GazeboSimpleHandControl::clearModelState(const std::string& name)
{
    std::unique_lock<std::mutex>(pub_mutex_);
    
    if(model_ms_map_.count(name))
        model_ms_map_.erase(name);
}

void GazeboSimpleHandControl::clearModelStates()
{
    std::unique_lock<std::mutex>(pub_mutex_);
    
    model_ms_map_.clear();
}

void GazeboSimpleHandControl::setLinearWaypoints(const std::vector< KDL::Frame >& ps, const std::vector< std::vector< double > >& js, const std::vector< double >& dts)
{
    stopLinWP();
    lin_wp_thread_ = new std::thread(&GazeboSimpleHandControl::setLinearWaypoints_impl, this, ps, js, dts);
}

void GazeboSimpleHandControl::stop()
{
    stopLinWP();
    stopPub();
}

void GazeboSimpleHandControl::stopLinWP()
{
    pub_mutex_.lock();
    // this will exit setLinearWPs thread
    lin_wp_stopping_ = true;
    pub_mutex_.unlock();
    
    if(lin_wp_thread_ != NULL && lin_wp_thread_->joinable())
    {
        lin_wp_thread_->join();
        delete lin_wp_thread_;
        lin_wp_thread_ = NULL;
    }
    
    lin_wp_stopping_ = false;
}

void GazeboSimpleHandControl::stopPub()
{
    pub_mutex_.lock();
    // this will exit recording thread
    pub_stopping_ = true;
    pub_mutex_.unlock();
    
    if(pub_thread_ != NULL && pub_thread_->joinable())
    {
        pub_thread_->join();
        delete pub_thread_;
        pub_thread_ = NULL;
    }
    
    pub_stopping_ = false;
}

void GazeboSimpleHandControl::setLinearWaypoints_impl(const std::vector< KDL::Frame >& ps, const std::vector< std::vector< double > >& js, const std::vector< double >& dts)
{
    // stop condition
    bool should_stop = false;
    trajectory_msgs::JointTrajectoryPoint pt;
    pt.time_from_start = ros::Duration(ts_);
    
    // wait a first ts_ before starting to publish anything
    ros::Time next_wake(ros::Time::now().toSec() + ts_);
    ROS_DEBUG_STREAM(CLASS_NAMESPACE << __func__ << " : initial value of next_wake = " << next_wake.toSec());
    
    if(ps.size() != js.size())
    {
        ROS_ERROR_STREAM(CLASS_NAMESPACE << __func__ << " : unequal size of poses waypoints and joint values waypoints - returning!");
        return;
    }
    
    for(int i=0; i<ps.size()-1; i++)
    {
        // initialize deltas
        const KDL::Frame &f1(ps.at(i)),&f2(ps.at(i+1));
        KDL::Frame f3;
        KDL::Twist t = KDL::diff(f1,f2);
        std::vector<double> deltaJ,currJ;
        for(int j=0; j<js.at(0).size(); j++)
            deltaJ.emplace_back(js.at(i+1).at(j) - js.at(i).at(j));
        
#if DEBUG
        std::cout << "for i=" << i << std::endl;
        std::cout << "f1 = " << f1 << std::endl;
        std::cout << "f2 = " << f2 << std::endl;
#endif
        
        // initialize joint values
        currJ = js.at(i);
        
        double curr_dts = dts.at(i);
        
        if((curr_dts / ts_) - (int)(curr_dts / ts_) != 0.0)
            ROS_WARN_STREAM(CLASS_NAMESPACE << __func__ << " : deltaT specified at index " << i << " (" << curr_dts << ") is NOT a multiple of the basic Ts specified in the constructor (" << ts_ << ") - the result will be approximated at the closest, smaller integer multiple of Ts.");
        
        // for as many Ts as we have in the current DeltaT
        for(int steps = 0; steps < curr_dts/ts_; steps++)
        {
            // stop condition
            pub_mutex_.lock();
            should_stop = lin_wp_stopping_;
            pub_mutex_.unlock();
            if(should_stop)
                return;
            
            // std::cout << "step #" << steps << " out of " << curr_dts/ts_ << " | curr_dts [s]: " << curr_dts << std::endl;
            f3 = KDL::addDelta(f1,t,steps*ts_/curr_dts);
            
            // compute timing and sleep for the remaining amount
            // NOTE that ros::Time is used because it needs to be aligned to simulation time!
            ros::Time tmp = ros::Time::now();
            ros::Duration sleep_dur(next_wake - tmp);
            if(sleep_dur.toSec() < 0.0)
                ROS_WARN_STREAM(CLASS_NAMESPACE << __func__ << " : in cycle i:" << i << ",j:" << steps << " NOT being able to sleep, remaining time negative (" << sleep_dur.toSec()*1000 << "ms)");
            else
            {
                sleep_dur.sleep();
                ROS_DEBUG_STREAM(CLASS_NAMESPACE << __func__ << " : in cycle i:" << i << ",j:" << steps << " sleeping for " << sleep_dur.toSec()*1000 << "ms");
            }
            next_wake += ros::Duration(ts_);
            ROS_DEBUG_STREAM("now: " << ros::Time::now().toSec() << " | next_wake: " << next_wake.toSec());
            
            // prepare stuff to publish (a thread will publish them)
            pub_mutex_.lock();
            tf::poseKDLToMsg(f3,ms_.pose);
            pt.positions = currJ;
            j_traj_.points.clear();
            j_traj_.points.push_back(pt);
            pub_mutex_.unlock();
            
            // increment current joint values at each timestep
            for(int j=0; j<currJ.size(); j++)
                currJ[j] += deltaJ[j]*ts_/curr_dts;
        }
    }
    
    // make sure to reach the final point
    pub_mutex_.lock();
    tf::poseKDLToMsg(ps.back(),ms_.pose);
    pt.positions = js.back();
    j_traj_.points.clear();
    j_traj_.points.push_back(pt);
    pub_mutex_.unlock();
}

void GazeboSimpleHandControl::publish_messages()
{
    bool should_stop = false;
    while(!should_stop)
    {
        pub_mutex_.lock();
        
        // publish the hand state using tf: it's the HoG plugin state
        static tf::TransformBroadcaster br;
        tf::Transform transform;
        transform.setOrigin( tf::Vector3(ms_.pose.position.x, ms_.pose.position.y, ms_.pose.position.z) );
        tf::Quaternion q(ms_.pose.orientation.x,ms_.pose.orientation.y,ms_.pose.orientation.z,ms_.pose.orientation.w);
        transform.setRotation(q);
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), ms_.reference_frame, ms_.model_name));
        
        // publish stuff
        j_traj_.header.stamp = ros::Time::now();
        joint_pub_.publish(j_traj_);
        for(auto obj_ms_:model_ms_map_)
            pose_pub_.publish(obj_ms_.second);
        // exit condition
        should_stop = pub_stopping_;
        pub_mutex_.unlock();
        
        // try to publish at 1kHz
        // NOTE: this is real time, not simulated one
        usleep(1000);
    }
}

