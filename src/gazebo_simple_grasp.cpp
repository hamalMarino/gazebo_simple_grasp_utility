/****************************************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Hamal Marino, Centro di Ricerca 'E. Piaggio', University of Pisa.
 *  
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************************/

#include "gazebo_simple_grasp_utility/gazebo_simple_grasp.h"
#include "gazebo_simple_grasp_utility/gazebo_simple_grasp_timing.h"

// ros various
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Trigger.h>
#include <std_srvs/Empty.h>
// utilities
#include <kdl_conversions/kdl_msg.h>
// interact with Gazebo
#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/ModelStates.h>
#include <gazebo_msgs/GetPhysicsProperties.h>
#include <gazebo_msgs/SetPhysicsProperties.h>
#include <gazebo_msgs/GetLinkProperties.h>
#include <gazebo_msgs/SetLinkProperties.h>
#include <gazebo_msgs/SpawnModel.h>
#include <gazebo_msgs/DeleteModel.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <gazebo_msgs/SetModelConfiguration.h>

#define OBJ_MODEL_NAME "object"
#define TEST_GRASP 0
#define SCREENSHOT_NAMESPACE std::string("/gazebo_screenshots_plugin/")
#define GSG_TIMING "gazebo_simple_grasp_timing"

GazeboSimpleGrasp::GazeboSimpleGrasp(double freq_multiplier):freq_multiplier_(freq_multiplier)
{
    link_sub = node_.subscribe<gazebo_msgs::LinkStates>("gazebo/link_states",10,&GazeboSimpleGrasp::callback_link,this);
    joint_sub = node_.subscribe<sensor_msgs::JointState>("gsgu_hand/joint_states",10,&GazeboSimpleGrasp::callback_joints,this);
    
    // read link_states, add a timeStamp, and publish them again using world_state
    my_world_pub = node_.advertise<gazebo_msgs::WorldState>("my_gazebo/world_state",10);
    screenshot_start_pub_ = node_.advertise<std_msgs::String>(SCREENSHOT_NAMESPACE + "start",10);
    screenshot_stop_pub_ = node_.advertise<std_msgs::Empty>(SCREENSHOT_NAMESPACE + "stop",10);
    screenshot_delete_client_ = node_.serviceClient<std_srvs::Trigger>(SCREENSHOT_NAMESPACE + "delete");
    
    // initialize various stuff
    init();
}

void GazeboSimpleGrasp::init()
{
    changePhysicsProperties(freq_multiplier_);
    deleteObject();
}

GazeboSimpleGrasp::~GazeboSimpleGrasp()
{
    // nothing to be done
}

void GazeboSimpleGrasp::setHandParameters(const std::string& pose_topic, const std::string& joint_topic, const std::vector< std::string >& joint_names, double ts)
{
    ts_ = ts;
    GSHC = boost::shared_ptr<GazeboSimpleHandControl>(new GazeboSimpleHandControl(pose_topic,joint_topic,joint_names,ts));
}

bool GazeboSimpleGrasp::recordBag(const gazebo_msgs::WorldState& ws, const sensor_msgs::JointState& js)
{
    bag_mutex.lock();
    
    static ros::Time last = ros::Time::now();
    ros::Time curr = ros::Time::now();
    count_wj++;
    bool ok = true;
    
    ros::Time wst = ws.header.stamp, jst = js.header.stamp;
    if(! (count_wj % 10))
        ROS_DEBUG_STREAM("World + Joints \tWJ:" << count_wj << "\tW:" << count_l << "\tJ:" << count_j << "\tDt[ms]:" << (wst - jst).toSec()*1000.0 << "\tfromLast[ms]:" << (curr - last).toSec()*1000.0);
    last = curr;
    
    // add a time information structure with: curr; wst; jst; wst - jst;
    gazebo_simple_grasp_utility::gazebo_simple_grasp_timing timing;
    timing.current = curr;
    timing.lst = wst;
    timing.jst = jst;
    
    // NOTE: there is a possible solution to this, although not very elegant...
    // terminate called after throwing an instance of 'ros::serialization::StreamOverrunException'
    // what():  Buffer Overrun
    // [GSGU-1] process has died [pid 18781, exit code -6, cmd /home/hamal/Code/catkin_ws/devel/lib/gazebo_simple_grasp_utility/gazebo_simple_grasp_utility __name:=GSGU __log:=/home/hamal/.ros/log/d746de78-6b6f-11e5-bb91-a0369f6c738a/GSGU-1.log].
    std::string e_occurrence(link_sub.getTopic());
    bool all_written = false;
    uint remaining_attempts = 1;
    // NOTE: even when possible exceptions are caught, the corresponding line is actually written; thus, don't write it once more...
    while(!all_written && remaining_attempts > 0)
        try
        {
            if(e_occurrence == link_sub.getTopic())
            {
                e_occurrence = joint_sub.getTopic();
                bag.write(link_sub.getTopic(), wst, ws);
            }
            if(e_occurrence == joint_sub.getTopic())
            {
                e_occurrence = GSG_TIMING;
                bag.write(joint_sub.getTopic(), jst, js);
            }
            if(e_occurrence == GSG_TIMING)
            {
                all_written = true;
                bag.write(GSG_TIMING, curr, timing);
            }
        }
        catch(ros::serialization::StreamOverrunException e)
        {
            remaining_attempts--;
            ROS_WARN_STREAM("Error writing \"" << e_occurrence << "\": " << e.what() << " | Trying again " << remaining_attempts << " more times...");
            // exit_code_ = 1;
            // ros::shutdown();
        }
        catch(...)
        {
            ROS_WARN_STREAM("Unexpected exception! writing \"" << e_occurrence << "\"...");
            remaining_attempts = 0;
        }
        
        if(remaining_attempts == 0)
        {
            ROS_ERROR_STREAM("Error writing \"" << e_occurrence << "\": Trying to restart the experiment...");
            ok = false;
        }
        
        bag_mutex.unlock();
        return ok;
}

void GazeboSimpleGrasp::callback_link(const gazebo_msgs::LinkStatesConstPtr& links)
{
    count_l++;
    
    // gazebo_msgs::WorldState ws;
    ws_buff.header.stamp = ros::Time::now();
    ws_buff.name = links->name;
    ws_buff.pose = links->pose;
    ws_buff.twist = links->twist;
    my_world_pub.publish(ws_buff);
}

void GazeboSimpleGrasp::callback_joints(const sensor_msgs::JointStateConstPtr& joints)
{
    count_j++;
    js_buff = *joints;
}

void GazeboSimpleGrasp::changePhysicsProperties(double freq_multiplier)
{
    // change Gazebo physics properties
    gazebo_msgs::GetPhysicsProperties get_phys_props;
    ros::service::call<gazebo_msgs::GetPhysicsProperties>("gazebo/get_physics_properties",get_phys_props);
    gazebo_msgs::SetPhysicsProperties set_phys_props;
    set_phys_props.request.gravity = get_phys_props.response.gravity;
    //set_phys_props.request.max_update_rate = get_phys_props.response.max_update_rate;
    set_phys_props.request.ode_config = get_phys_props.response.ode_config;
    //set_phys_props.request.time_step = get_phys_props.response.time_step;
    set_phys_props.request.ode_config.contact_surface_layer = 0.001;
    set_phys_props.request.ode_config.max_contacts = 100;
    set_phys_props.request.time_step = 0.001/freq_multiplier;
    set_phys_props.request.max_update_rate = 1000.0*freq_multiplier;
    // // NOTE: as updated parameters didn't show up in the client gui (it's just graphics, it works if you relaunch gzclient), I made this simple test, and it works
    // set_phys_props.request.gravity.z *= -1.0;
    ros::service::call<gazebo_msgs::SetPhysicsProperties>("gazebo/set_physics_properties",set_phys_props);
}

void GazeboSimpleGrasp::spawnObject(const std::string& object_xml, const KDL::Frame& init_pose, bool add_back_gravity)
{
    // spawn object
    gazebo_msgs::SpawnModel spawner;
    spawner.request.robot_namespace = "object";
    spawner.request.reference_frame = "world";
    tf::poseKDLToMsg(init_pose,spawner.request.initial_pose);
    spawner.request.model_name = OBJ_MODEL_NAME;
    spawner.request.model_xml = object_xml;
    ros::service::call<gazebo_msgs::SpawnModel>("gazebo/spawn_urdf_model",spawner);
    
    // add again gravity for the object (the HoG plugin removes it)
    if(add_back_gravity)
    {
        gazebo_msgs::GetLinkProperties get_link_props;
        get_link_props.request.link_name = "object::object";
        ros::service::call<gazebo_msgs::GetLinkProperties>("gazebo/get_link_properties",get_link_props);
        gazebo_msgs::SetLinkProperties set_link_props;
        set_link_props.request.com = get_link_props.response.com;
        set_link_props.request.ixx = get_link_props.response.ixx;
        set_link_props.request.ixy = get_link_props.response.ixy;
        set_link_props.request.ixz = get_link_props.response.ixz;
        set_link_props.request.iyy = get_link_props.response.iyy;
        set_link_props.request.iyz = get_link_props.response.iyz;
        set_link_props.request.izz = get_link_props.response.izz;
        set_link_props.request.link_name = get_link_props.request.link_name;
        set_link_props.request.mass = get_link_props.response.mass;
        set_link_props.request.gravity_mode = true;
        ros::service::call<gazebo_msgs::SetLinkProperties>("gazebo/set_link_properties",set_link_props);
    }
}

void GazeboSimpleGrasp::deleteObject()
{
    // delete object model
    gazebo_msgs::DeleteModel delmdl;
    delmdl.request.model_name = OBJ_MODEL_NAME;
    ros::service::call<gazebo_msgs::DeleteModel>("gazebo/delete_model",delmdl);
}

bool GazeboSimpleGrasp::doSingleGraspRun(const std::string& bag_name, const std::vector< KDL::Frame >& vec_frames, const std::vector< std::vector< double > >& vec_joints, const std::vector< double >& dts)
{
    gazebo_msgs::ModelState ms;
    ms.model_name = "hand_hog_desired";
    ms.reference_frame = "world";
    tf::poseKDLToMsg(vec_frames.front(),ms.pose);
    gazebo_msgs::ModelState obj_ms,hand_ms;
    obj_ms.model_name = "object";
    obj_ms.reference_frame = "world";
    tf::poseKDLToMsg(object_init_pose_,obj_ms.pose);
    hand_ms.model_name = "gsgu_hand";
    hand_ms.reference_frame = "world";
    tf::poseKDLToMsg(vec_frames.front(),hand_ms.pose);
    
    std_srvs::Empty empty;
    // NOTE: cannot call reset_simulation, as the joint states publisher gets disconnected!
    ros::service::call<std_srvs::Empty>("gazebo/reset_world",empty);
    
    GSHC->setModelState(hand_ms.model_name,hand_ms);
    GSHC->setModelState(obj_ms.model_name,obj_ms);
    GSHC->setHandState(ms,vec_joints.front());
    
    // the hand may have assumed a strange configuration in simulation: reset it via joint value assignment
    gazebo_msgs::GetModelProperties gmp;
    gmp.request.model_name = "gsgu_hand";
    ros::service::call<gazebo_msgs::GetModelProperties>("gazebo/get_model_properties",gmp);
    gazebo_msgs::SetModelConfiguration smc;
    smc.request.model_name = gmp.request.model_name;
    smc.request.joint_names = gmp.response.joint_names;
    smc.request.joint_positions = std::vector<double>(smc.request.joint_names.size(),0.0);
    ros::service::call<gazebo_msgs::SetModelConfiguration>("gazebo/set_model_configuration",smc);
    
    ros::service::call<std_srvs::Empty>("gazebo/unpause_physics",empty);
    
    double init_time = ros::Time::now().toSec();
    // do nothing until clock gets published...
    while(ros::Time::now().toSec() < init_time + 0.5)
        usleep(5000);
    
    bag.open(bag_name + ".bag", rosbag::bagmode::Write);
    bag.setCompression(rosbag::CompressionType::BZ2);
    // to avoid overrun errors, use 5KB chunks (default was 768KB)
    // TODO: this doesn't always work: try to understand how to fix this...
    bag.setChunkThreshold(5*1024);
    
    char y;
    ROS_DEBUG_STREAM("Make sure the hand has finished positioning in the desired frame. Then, press any key to start recording in the bag file \'" << bag_name << "\'...");
    // std::cin >> y;
    ros::Duration(0.1).sleep();
    
    // start taking screenshots
    std_msgs::String screenshot_name;
    screenshot_name.data = bag_name;
    screenshot_start_pub_.publish(screenshot_name);
    
    // init/reset variables
    GSHC->clearModelState(hand_ms.model_name);
    GSHC->clearModelState(obj_ms.model_name);
    ros::Duration total_exp_time(0.5*freq_multiplier_);
    for(auto d:dts)
        total_exp_time += ros::Duration(d);
    count_j = 0;
    count_l = 0;
    count_wj = 0;
    bool repeat_trial = false;
    
    // NOTE: this is threaded
    GSHC->setLinearWaypoints(vec_frames,vec_joints,dts);
    // NOTE: this does not need to be threaded, as control part is...
    // use 0.5 more seconds, just to be sure everything finished correctly
    // NOTE: increased when using small time-steps (e.g. 5.0sec when Ts=0.0001sec)
    ros::Time final_time = ros::Time::now();
    final_time += total_exp_time;
    
    ros::Rate r(1.0/ts_);
    while(ros::ok() && ros::Time::now() < final_time)
    {
        // exit the cycle if something went wrong
        if(!recordBag(ws_buff,js_buff))
        {
            repeat_trial = true;
            break;
        }
        r.sleep();
    }
    
    bag.close();
    
    // NOTE: if the number of recorded messages strongly disagree with the expected amount (+/-5%), invalid the experiment
    if(count_wj*ts_ < 0.95*total_exp_time.toSec() || count_wj*ts_ > 1.05*total_exp_time.toSec())
        repeat_trial = true;
    
    // stop taking screenshots
    screenshot_stop_pub_.publish(std_msgs::Empty());
    
    if(repeat_trial)
    {
        ROS_WARN_STREAM("Unable to record the bag correctly (recorded " << count_wj << " msgs for an experiment with total duration " << total_exp_time.toSec() << "s...");
        // delete last bunch of screenshots
        std_srvs::Trigger trig;
        if(!screenshot_delete_client_.call(trig))
            ROS_WARN_STREAM("Unable to delete the screenshots: remember to do that manually!");
        else if(!trig.response.success)
            ROS_WARN_STREAM("Unable to delete the screenshots: remember to do that manually! Service response was: " << trig.response.message);
    }
    else
        ROS_INFO_STREAM("Recording stopped, bag closed, done for now! Recorded " << count_wj << " msgs for an experiment with total duration " << total_exp_time.toSec() << "s");
    
    #if TEST_GRASP
    if(!repeat_trial)
    {
        // NOTE: add a higher waypoint: this could be used for testing whether the grasp was successful
        KDL::Frame up_frame(vec_frames.back());
        up_frame.p.data[2] += 0.5;
        double last_wp = 5.0;
        GSHC->setLinearWaypoints(std::vector<KDL::Frame>({vec_frames.back(),up_frame}), std::vector<std::vector<double>>({vec_joints.back(),vec_joints.back()}), std::vector<double>({last_wp}));
        ros::Duration(last_wp).sleep();
    }
    #endif
    
    // bring back the hand to the initial position
    GSHC->setHandState(ms,vec_joints.front());
    
    // stop all and wait for it to take effect (threads need to exit, and for this ros::Time must be published)
    GSHC->stop();
    ros::Duration(0.1).sleep();
    ros::service::call<std_srvs::Empty>("gazebo/pause_physics",empty);
    
    return (!repeat_trial);
}

bool GazeboSimpleGrasp::setObjectByName(std::string obj_name, const KDL::Frame& object_init_pose, bool add_back_gravity)
{
    // first, delete previous object (if any)
    deleteObject();
    // then, spawn object
    std::string object_xml;
    node_.getParam(obj_name + "_description",object_xml);
    spawnObject(object_xml,KDL::Frame::Identity(),add_back_gravity);
    object_init_pose_ = object_init_pose;
}

